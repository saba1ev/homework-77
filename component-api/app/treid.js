const express = require('express');
const multer = require('multer');
const path = require('path')
const nanoid = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, config.uploadPath)
  },
  filename: (req, file, cb) =>{
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage})

const router = express.Router();

router.get('/', (req, res)=>{
  console.log('bla');
    res.send(db.fetchTreid());
});

router.post('/', upload.single('image'), (req, res) => {
  const message = req.body;
  console.log(message);
  if(req.file) {
    message.image = req.file.filename
  }

  if(req.body.text !== '') {
    if(req.body.author === '') {
      message.author = 'Anonymous'
    }
    message.date = new Date().toISOString();
    message.id = nanoid();
    db.addTreid(message);
    res.send({message: 'OK'})
  } else {
    res.status(400).send({message: 'You must enter message'})
  }
});

module.exports = router;