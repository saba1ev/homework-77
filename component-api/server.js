const express = require('express');
const cors = require('cors');
const db = require('./fileDb');
const treid = require('./app/treid');

const app = express();
const port = 8000;


db.init();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

app.use('/messages', treid);

app.listen(port, () =>{
  console.log(`http://localhost:${port}`)
});