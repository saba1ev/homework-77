const fs = require('fs');


const fileName = './db.json';

let treids = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      treids = JSON.parse(fileContents);
    } catch (event) {
      treids = [];
    }
  },
  fetchTreid() {
    return treids;
  },
  addTreid(treid) {
    treids.push(treid);
    this.save();
  },
  save() {
    fs.writeFileSync(fileName, JSON.stringify(treids, null, 2))
  },
};