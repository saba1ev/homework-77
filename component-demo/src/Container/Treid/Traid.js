import React, {Component, Fragment} from 'react';


import './Treid.css'
import TraidInfo from "../../components/TreidInfo/TraidInfo";
import {Button, Container, Input} from "reactstrap";
import {connect} from "react-redux";
import {fetchTreid, postTreid} from "../../store/actions/actions";

class Traid extends Component {

  state = {
    author: '',
    text: '',
    image: null,
  };


  componentDidMount(){
    this.props.get()
  }

  submitHendler = (event) =>{
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key])
    });

    this.props.post(formData);
  };


  inputChangeHenlder = event =>{
    return this.setState({
      [event.target.name]: event.target.value
    })
  };
  fileChangeHendler = event =>{
    return this.setState({
      [event.target.name]: event.target.files[0]
    })
  };


  render() {
    return (
      <Fragment>
        <TraidInfo messages={this.props.state.treid}/>
        <Container>
          <div className='Form-Input'>
            <Input name='author'
                   placeholder='Введите свое имя'
                   value={this.state.author}
                   onChange={(event)=>this.inputChangeHenlder(event)}
            />
            <Input name='text'
                   type='textarea'
                   placeholder='Введите текст'
                   value={this.state.text}
                   onChange={(event)=>this.inputChangeHenlder(event)}
            />
            <Input name='image' type='file' onChange={(event)=>this.fileChangeHendler(event)} />
            <Button color='success' onClick={(event)=>this.submitHendler(event)}>save</Button>
          </div>
        </Container>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) =>{
  return{
    state: state
  }
};
const mapDispatchToProps = (dispatch) =>{
  return{
    post: (data)=>dispatch(postTreid(data)),
    get: ()=>dispatch(fetchTreid())
  }
};
export default connect(mapStateToProps, mapDispatchToProps) (Traid);