import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import {Alert, Container} from "reactstrap";
import Traid from "./Container/Treid/Traid";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path='/' exact component={Traid}/>
        <Route path='' render={()=> (
          <Container>
            <Alert color='danger'>Page Not Found</Alert>
          </Container>
        )}/>
      </Switch>
    );
  }
}

export default App;