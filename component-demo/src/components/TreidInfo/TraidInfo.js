import React from 'react';
import './TraidInfo.css';
import {Card, CardImg, CardText, CardTitle, Container} from "reactstrap";

const TraidInfo = (props) => {
  return (
    <Container>
      {props.messages && props.messages.map((message, index) => {
        return (
          <Card className='card' key={index}>
            <CardImg onError={error => error.target.style.display = 'none'} top width="100%" src={"http://localhost:8000/uploads/" + message.image}/>
            <CardTitle>{message.author}</CardTitle>
            <CardText>{message.text}</CardText>
          </Card>
        )
      })}
    </Container>
  );
};

export default TraidInfo;