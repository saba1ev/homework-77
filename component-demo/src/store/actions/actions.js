import {
  FETCH_TREID_FAILURE,
  FETCH_TREID_REQUEST,
  FETCH_TREID_SUCCESS,
} from "./actionTypes";
import axios from '../../axios-api'


export const fetch_treid_request = () => (
  {type: FETCH_TREID_REQUEST}
);
export const fetch_treid_success = (treid) => (
  {type: FETCH_TREID_SUCCESS, treid}
);
export const fetch_tried_failure = (error) => (
  {type: FETCH_TREID_FAILURE, error}
);

export const fetchTreid = () =>{
  return (dispatch)=>{
    dispatch(fetch_treid_request());
    axios.get('/messages').then(res=>{
      dispatch(fetch_treid_success(res.data))
    }, error =>{
      dispatch(fetch_tried_failure(error))
    })
  }
};

export const postTreid = (post) =>{
  return dispatch =>{
    dispatch(fetch_treid_request());
    axios.post('/messages', post).then(()=>dispatch(fetchTreid()))
  }
}


