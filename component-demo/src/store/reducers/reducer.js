import {FETCH_TREID_FAILURE, FETCH_TREID_REQUEST, FETCH_TREID_SUCCESS} from "../actions/actionTypes";

const initialState = {
  treid: null,
  loading: false,
  error: null
};

const reducer = (state = initialState, action)=>{
  switch (action.type) {
    case FETCH_TREID_REQUEST:
      return{...state, loading: true};
    case FETCH_TREID_SUCCESS:
      return{...state, treid: action.treid, loading: false};
    case FETCH_TREID_FAILURE:
      return{...state, loading: false, error: action.error};
    default:
      return state
  }
};

export default reducer;